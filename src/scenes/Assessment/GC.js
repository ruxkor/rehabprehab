import React from 'react';
import { connect } from 'react-redux';
import { StyleSheet, Text } from 'react-native';

import Container from '../../components/Container';
// const foo = require('../../healthcloud_sdk')

const styles = StyleSheet.create({
});

const AssessmentGCScene = () => (
  <Container>
    <Text>AssessmentGCScene</Text>
  </Container>
);

AssessmentGCScene.navigationOptions = {
  title: 'AssessmentGCScene',
};

const mapStateToProps = state => ({
});

const mapDispatchToProps = dispatch => ({
});

export default connect(mapStateToProps, mapDispatchToProps)(AssessmentGCScene);