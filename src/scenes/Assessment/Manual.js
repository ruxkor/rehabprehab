import React from 'react';
import { connect } from 'react-redux';
import { StyleSheet, Text } from 'react-native';

import Container from '../../components/Container';

const styles = StyleSheet.create({
});

const AssessmentManualScene = () => (
  <Container>
    <Text>AssessmentManualScene</Text>
  </Container>
);

AssessmentManualScene.navigationOptions = {
  title: 'AssessmentManualScene',
};

const mapStateToProps = state => ({
});

const mapDispatchToProps = dispatch => ({
});

export default connect(mapStateToProps, mapDispatchToProps)(AssessmentManualScene);