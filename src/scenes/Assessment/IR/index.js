import React from 'react';
import { connect } from 'react-redux';
import { StyleSheet, View, Image, Text } from 'react-native';

import Container from '../../../components/Container';

const styles = StyleSheet.create({
  header: {
    flex: 1,
  },
  assessmentImage: {
    flex: 7,
  }
});

const AssessmentIRScene = () => (
  <Container>
    <View style={styles.header}>
      <Text>Some assessment Text</Text>   
    </View>
    <Image style={styles.assessmentImage} source={require('./assets/scan.png')}>
      <Text>some text on top of the image</Text>
    </Image>
  </Container>
);

AssessmentIRScene.navigationOptions = {
  title: 'AssessmentIRScene',
};

const mapStateToProps = state => ({
});

const mapDispatchToProps = dispatch => ({
});

export default connect(mapStateToProps, mapDispatchToProps)(AssessmentIRScene);