import React from 'react';
import { connect } from 'react-redux';
import { StyleSheet, Text, Button } from 'react-native';
import { NavigationActions } from 'react-navigation';

import Container from '../../components/Container';

const styles = StyleSheet.create({
});

const AssessmentScene = ({assessment, assessmentGC, assessmentIR, assessmentManual}) => (
  <Container>
    <Text>AssessmentScene</Text>
    <Button onPress={assessment} title="Test" />
    <Button onPress={assessmentIR} title="Body Scan" />
    <Button onPress={assessmentGC} title="Gesundheitscloud" />
    <Button onPress={assessmentManual} title="Manual" />
  </Container>
);

AssessmentScene.navigationOptions = {
  title: 'AssessmentScene',
};

const mapStateToProps = state => ({
  ui: 2
});

const mapDispatchToProps = dispatch => ({
  assessment: () => dispatch(NavigationActions.navigate({ routeName: 'Assessment' })),
  assessmentIR: () => dispatch(NavigationActions.navigate({ routeName: 'AssessmentIR' })),
  assessmentManual: () => dispatch(NavigationActions.navigate({ routeName: 'AssessmentManual' })),
  assessmentGC: () => dispatch(NavigationActions.navigate({ routeName: 'AssessmentGC' })),
});

export default connect(mapStateToProps, mapDispatchToProps)(AssessmentScene);