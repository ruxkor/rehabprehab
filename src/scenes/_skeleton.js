import React from 'react';
import { connect } from 'react-redux';
import { StyleSheet, Text } from 'react-native';

import Container from '../components/Container';

const styles = StyleSheet.create({
});

const _skeleton = () => (
  <Container>
    <Text>_skeleton</Text>
  </Container>
);

_skeleton.navigationOptions = {
  title: '_skeleton',
};

const mapStateToProps = state => ({
});

const mapDispatchToProps = dispatch => ({
});

export default connect(mapStateToProps, mapDispatchToProps)(_skeleton);