import React from 'react';
import { StyleSheet, Text, View } from 'react-native';
import consts from '../consts';

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: consts.backgroundColor,
  },
  welcome: {
    fontSize: 20,
    textAlign: 'center',
    color: '#ffffff',
    margin: 30,
  },
}); 

const ProfileScreen = () => (
  <View style={styles.container}>
    <Text style={styles.welcome}>
      Sara:
    </Text>
    <Text style={styles.welcome}>
      You today did 2 sessions with 123 repetitions.
    </Text>
    <Text style={styles.welcome}>
      Gesundheitsclient Account: Sara
    </Text>
  </View>
);

ProfileScreen.navigationOptions = {
  title: 'Profile',
};

export default ProfileScreen;
