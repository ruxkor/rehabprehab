import React from 'react';
import PropTypes from 'prop-types';
import { StyleSheet, View } from 'react-native';
import consts from '../consts';

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: consts.backgroundColor,
  },
});

const Container = (props) => (
  <View style={styles.container}>
    {props.children}
  </View>
);

Container.propTypes = {
  children: PropTypes.node
};

Container.navigationOptions = {
  title: 'Log In',
};

export default Container;