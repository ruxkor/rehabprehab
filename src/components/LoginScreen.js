import React from 'react';
import PropTypes from 'prop-types';
import { Button, StyleSheet, Text, View } from 'react-native';
import Container from './Container';

const styles = StyleSheet.create({
  welcome: {
    fontSize: 30,
    textAlign: 'center',
    color: '#ffffff',
    margin: 30,
  },
});

const LoginScreen = ({ navigation }) => (
  <Container>
    <Text style={styles.welcome}>
      Welcome to ReHab!
    </Text>
    <Button
      onPress={() => navigation.dispatch({ type: 'Login' })}
      title="Log in"
    />
  </Container>
);

LoginScreen.propTypes = {
  navigation: PropTypes.object.isRequired,
};

LoginScreen.navigationOptions = {
  title: 'Log In',
};

export default LoginScreen;
