import React from 'react';
import { connect } from 'react-redux';
import { StyleSheet, Button } from 'react-native';
import { NavigationActions } from 'react-navigation';

import LoginStatusMessage from './LoginStatusMessage';
import AuthButton from './AuthButton';

import Container from './Container';

const styles = StyleSheet.create({
});

const MainScreen = ({ assessment}) => {
  return (
    <Container>
      <LoginStatusMessage />
      <Button onPress={assessment} title="Assessment" />
      <AuthButton />
    </Container>
  );
}

MainScreen.navigationOptions = {
  title: 'MainScreen',
};

const mapStateToProps = state => ({
  ui: 2
});

const mapDispatchToProps = dispatch => ({
  assessment: () => dispatch(NavigationActions.navigate({ routeName: 'Assessment' })),
});

export default connect(mapStateToProps, mapDispatchToProps)(MainScreen);