/**
 * @flow
 */

import React from 'react';
import { AppRegistry, AsyncStorage, StyleSheet, Text, View } from 'react-native';
import { Provider } from 'react-redux';

import { createStore, compose } from 'redux';
import {persistStore, autoRehydrate} from 'redux-persist';

import AppReducer from './src/reducers';
import AppWithNavigationState from './src/navigators/AppNavigator';

class rehabprehabApp extends React.Component {
  // store = compose(autoRehydrate())(createStore)(AppReducer);
  store = createStore(AppReducer);
  constructor() {
    super();
    this.state = { rehydrated: false };
  }
  componentWillMount() {
    this.setState({ rehydrated: true });
    // persistStore(this.store, {storage: AsyncStorage}, () => {
    //   this.setState({ rehydrated: true });
    // });
  }
  
  render() {
    if (!this.state.rehydrated) {
      return (
        <View style={styles.container}>
          <Text style={styles.welcome}>Loading...</Text>
        </View>
      );
    }
    return (
      <Provider store={this.store}>
        <AppWithNavigationState />
      </Provider>
    );
  }
}

AppRegistry.registerComponent('rehabprehab', () => rehabprehabApp);

export default rehabprehabApp;
